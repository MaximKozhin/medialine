<?php

use yii\db\Migration;

/**
 * Class m200823_081541_article_rubric
 */
class m200823_081541_article_rubric extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article_rubric}}',[
            'article_id'    => $this->integer(10)->unsigned()->comment('Статья'),
            'rubric_id'     => $this->integer(10)->unsigned()->comment('Рубрика'),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);

        $this->addPrimaryKey('pk-article_rubric', '{{%article_rubric}}', ['article_id', 'rubric_id']);
        $this->addForeignKey('fk-article_rubric-article', '{{%article_rubric}}', 'article_id', '{{%article}}', 'id');
        $this->addForeignKey('fk-article_rubric-rubric', '{{%article_rubric}}', 'rubric_id', '{{%rubric}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-article_rubric-article', '{{%article_rubric}}');
        $this->dropForeignKey('fk-article_rubric-rubric', '{{%article_rubric}}');
        $this->dropTable('{{%article_rubric}}');
    }
}
