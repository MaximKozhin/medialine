<?php

use yii\db\Migration;

/**
 * Class m200823_081232_article
 */
class m200823_081232_article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article}}',[
            'id'            => $this->primaryKey(10)->unsigned(),
            'author_id'     => $this->integer(10)->unsigned()->comment('Автор статьи'),
            'image'         => $this->string(255)->comment('Изображение статьи'),
            'name'          => $this->string(255)->notNull()->comment('Заголовок статьи'),
            'body'          => $this->text()->comment('Тело статьи'),
            'deleted'       => $this->boolean()->defaultValue(0)->comment('Удалено'),
            'created_at'    => $this->integer(10)->unsigned()->comment('Создано'),
            'updated_at'    => $this->integer(10)->unsigned()->comment('Изменено'),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);

        $this->createIndex('index-article-deleted', '{{%article}}', 'deleted');
        $this->addForeignKey('fk-article-author', '{{%article}}', 'author_id', '{{%identity}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-article-author', '{{%article}}');
        $this->dropTable('{{%article}}');
    }
}
