<?php

use yii\db\Migration;

/**
 * Class m200823_080842_rubric_tree
 */
class m200823_080842_rubric_tree extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rubric_tree}}',[
            'rubric_id'     => $this->integer(10)->unsigned()->comment('Рубрика'),
            'parent_id'     => $this->integer(10)->unsigned()->comment('Родительская рубрика'),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);

        $this->addPrimaryKey('pk-rubric_tree', '{{%rubric_tree}}', ['rubric_id', 'parent_id']);
        $this->addForeignKey('fk-rubric_tree-rubric', '{{%rubric_tree}}', 'rubric_id', '{{%rubric}}', 'id');
        $this->addForeignKey('fk-rubric_tree-parent', '{{%rubric_tree}}', 'parent_id', '{{%rubric}}', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-rubric_tree-rubric', '{{%rubric_tree}}');
        $this->dropForeignKey('fk-rubric_tree-parent', '{{%rubric_tree}}');
        $this->dropTable('{{%rubric_tree}}');
    }
}
