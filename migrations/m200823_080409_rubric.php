<?php

use yii\db\Migration;

/**
 * Class m200823_080409_rubric
 */
class m200823_080409_rubric extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rubric}}',[
            'id'            => $this->primaryKey(10)->unsigned(),
            'parent_id'     => $this->integer(10)->unsigned()->comment('Родительская рубрика'),
            'level'         => $this->integer(10)->unsigned()->comment('Уровень вложенности'),
            'image'         => $this->string(255)->comment('Изображение рубрики'),
            'name'          => $this->string(255)->notNull()->comment('Наименование рубрики'),
            'deleted'       => $this->boolean()->defaultValue(0)->comment('Удалено'),
            'created_at'    => $this->integer(10)->unsigned()->comment('Создано'),
            'updated_at'    => $this->integer(10)->unsigned()->comment('Изменено'),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);

        $this->createIndex('index-rubric-deleted', '{{%rubric}}', 'deleted');
        $this->addForeignKey('fk-rubric-parent', '{{%rubric}}', 'parent_id', '{{%rubric}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-rubric-parent', '{{%rubric}}');
        $this->dropTable('{{%rubric}}');
    }
}
