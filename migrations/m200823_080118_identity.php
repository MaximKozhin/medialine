<?php

use yii\db\Migration;

/**
 * Class m200823_080118_identity
 */
class m200823_080118_identity extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \yii\db\Exception|\yii\base\Exception
     */
    public function safeUp()
    {
        $this->createTable('{{%identity}}', [
            'id'            => $this->primaryKey(10)->unsigned(),
            'username'      => $this->string(100)->notNull()->unique()->comment('Логин пользователя'),
            'password'      => $this->string(100)->notNull()->comment('Хэш пароля'),
            'auth_key'      => $this->string(100)->notNull()->unique()->comment('Ключ аутентификации'),
            'deleted'       => $this->boolean()->defaultValue(0)->comment('Удалено'),
            'created_at'    => $this->integer(10)->unsigned()->comment('Создано'),
            'updated_at'    => $this->integer(10)->unsigned()->comment('Изменено'),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);

        $this->createIndex('index-identity-deleted', '{{%identity}}', 'deleted');

        Yii::$app->db->createCommand()->insert('{{%identity}}', [
            'username' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('admin'),
            'auth_key' => Yii::$app->security->generateRandomString(32),
            'deleted' => 0,
            'created_at' => time(),
            'updated_at' => time(),
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%identity}}');
    }
}
