<?php

namespace app\modules\api\controllers;

use app\models\db\Article;
use app\models\db\ArticleRubric;
use app\models\db\Rubric;
use Yii;
use app\models\db\RubricTree;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ArticleController
 * @package app\modules\api\controllers
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => Article::class,
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => Article::class
            ],
            'rubrics' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => Rubric::class,
                'prepareDataProvider' => function () {
                    if(key_exists('id', Yii::$app->request->queryParams) and ($id = Yii::$app->request->queryParams['id'])) {
                        /** Ищем статью */
                        if(($article = Article::findOne(['id' => $id, 'deleted' => false])) === null) {
                            throw new NotFoundHttpException('Article was not found');
                        }
                        /** Получам рубрики статьи */
                        return Rubric::find()
                            ->where(['deleted' => false, 'id' => ArticleRubric::find()
                                ->select('rubric_id')
                                ->where(['article_id' => $article->id])
                                ->column()
                            ])->all();
                    } else {
                        throw new BadRequestHttpException("Parameter 'id' must be set");
                    }
                }
            ]
        ];
    }
}