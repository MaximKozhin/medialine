<?php

namespace app\modules\api\controllers;

use app\models\db\Article;
use app\models\db\ArticleRubric;
use Yii;
use app\models\db\Rubric;
use app\models\db\RubricTree;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class RubricController
 * @package app\modules\api\controllers
 */
class RubricController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => Rubric::class,
                'prepareDataProvider' => function () {
                    return Rubric::find()->where(['deleted' => false])->all();
                }
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => Rubric::class,
                'prepareDataProvider' => function () {
                    return Rubric::find()->where(['deleted' => false])->one();
                }
            ],
            'tree' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => RubricTree::class,
                'prepareDataProvider' => function () {
                    return RubricTree::find()->where(Yii::$app->request->queryParams)->all();
                }
            ],
            'articles' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => Article::class,
                'prepareDataProvider' => function () {
                    if(key_exists('id', Yii::$app->request->queryParams) and ($id = Yii::$app->request->queryParams['id'])) {
                        /** Ищем рубрику */
                        if(($rubric = Rubric::findOne(['id' => $id, 'deleted' => false])) === null) {
                            throw new NotFoundHttpException('Rubric was not found');
                        }
                        /** Получам статьи по идентификаторам */
                        return Article::find()
                            ->where(['deleted' => false, 'id' => ArticleRubric::find()
                                ->select('article_id')
                                ->where(['rubric_id' => array_merge($rubric->getRubricTree()
                                    ->joinWith(['rubric'])
                                    ->select('rubric_id')
                                    ->where(['deleted' => false])
                                    ->column(), [$rubric->id])])
                                ->column()
                            ])->orderBy(['updated_at' => SORT_DESC])->all();
                    } else {
                        throw new BadRequestHttpException("Parameter 'id' must be set");
                    }
                }
            ],
        ];
    }
}