<?php

namespace app\modules\main\controllers;

use app\models\db\Article;
use app\models\db\ArticleRubric;
use app\models\db\Rubric;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class DefaultController
 * @package app\modules\main\controllers
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Главная страница с выводом только корневых рубрик и всех новостей по дате их последнего изменения
     * @return string
     */
    public function actionIndex()
    {
        /**
         * Ищем только корневые рубрики
         * @var Rubric[] $rubrics
         */
        $rubrics = Rubric::find()->where(['deleted' => false, 'parent_id' => null])->orderBy(['name' => SORT_ASC])->all();

        /**
         * щем первые 10 статей по дате последнего изменения
         * @var Article[] $articles
         */
        $articles = Article::find()
            ->with(['author', 'rubrics'])
            ->where(['deleted' => false])
            ->orderBy(['updated_at' => SORT_DESC])
            ->limit(10)
            ->all();

        /** Формируем хоебные крошки */
        $this->view->title = Yii::$app->name;

        return $this->render('index',[
            'rubrics' => $rubrics,
            'articles' => $articles,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionRubric($id)
    {
        $rubric = $this->findRubric($id);

        /** Формируем идентификаторы всех дочерних рубрик, которые не удалены */
        $rubricIdentifiers = $rubric->getRubricTree()
            ->joinWith(['rubric'])
            ->select('rubric_id')
            ->where(['deleted' => false])
            ->column();

        /** И затем добавляем нашу рубрику */
        $rubricIdentifiers[] = $rubric->id;

        /** Получаем идентификаторы статей по найденным рубрикам */
        $articleIdentifiers = ArticleRubric::find()
            ->select('article_id')
            ->where(['rubric_id' => $rubricIdentifiers])
            ->column();

        /** запрос на получение статей */
        $query = Article::find()
            ->with(['author', 'rubrics'])
            ->where(['deleted' => false, 'id' => $articleIdentifiers])
            ->orderBy(['updated_at' => SORT_DESC]);


        /** Модель пагинации для постраничной разбивки */
        $pagination = new Pagination([
            'totalCount' => $query->count(),
            'pageSize' => Yii::$app->params['article']['limit']
        ]);

        /** Получам статьи по идентификаторам */
        $articles = $query->limit($pagination->limit)->offset($pagination->offset)->all();

        /** Берем первого родителя */
        $parent = $rubric->parent;
        $breadcrumbs = [];

        /** Перебираем родителей рубрики */
        while($parent) {

            /** Каждого родителя добавляем в массив */
            $breadcrumbs[] = $parent;

            /** Переходим к следующей рубрике */
            $parent = $parent->parent;
        }

        /** Перебираем массив рубрик в обратном порядке */
        foreach(array_reverse($breadcrumbs) as $breadcrumb) {
            $this->view->params['breadcrumbs'][] = ['label' => $breadcrumb->name, 'url' => ['rubric', 'id' => $breadcrumb->id]];
        }

        /** Добавляем крошку текущей рубрики */
        $this->view->title = $rubric->name;
        $this->view->params['breadcrumbs'][] = ['label' => $this->view->title];

        return $this->render('rubric', [
            'rubric' => $rubric,
            'articles' => $articles,
            'pagination' => $pagination
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $article = $this->findArticle($id);

        /** Формируем хоебные крошки */
        $this->view->title = $article->name;
        $this->view->params['breadcrumbs'][] = ['label' => Yii::$app->name, 'url' => Yii::$app->defaultRoute];
        $this->view->params['breadcrumbs'][] = ['label' => $this->view->title];

        return $this->render('view', [
            'article' => $article
        ]);
    }

    /**
     * @param $id
     * @return Rubric|null
     * @throws NotFoundHttpException
     */
    protected function findRubric($id)
    {
        if (($rubric = Rubric::findOne(['id' => $id, 'deleted' => false])) !== null) {
            return $rubric;
        }
        throw new NotFoundHttpException('Рубрика не найдена');
    }

    /**
     * @param $id
     * @return Article|null
     * @throws NotFoundHttpException
     */
    protected function findArticle($id)
    {
        if (($article = Article::findOne(['id' => $id, 'deleted' => false])) !== null) {
            return $article;
        }
        throw new NotFoundHttpException('Статья не найдена');
    }
}