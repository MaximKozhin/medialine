<?php

use app\models\db\Article;
use app\models\db\Rubric;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $rubrics Rubric[] */
/* @var $articles Article[] */

?>
<div class="main-index">
    <h1 class="no-margin clearfix"><?=Html::encode($this->title)?></h1>
    <?php if($rubrics):?>
        <hr>
        <?=$this->render('_rubrics', ['rubrics' => $rubrics])?>
    <?php endif?>
    <br>
    <?php if($articles):?>
        <?=$this->render('_articles', ['articles' => $articles])?>
    <?php else:?>
        <div class="alert alert-warning no-margin">Сюда пока никто не добавил новостей</div>
    <?php endif?>
</div>