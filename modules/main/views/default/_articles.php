<?php

/* @var $articles Article[] */

use app\models\db\Article;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php foreach ($articles as $article):?>
    <div class="media article-preview">
        <div class="media-left">
            <a href="<?=Url::to(['view', 'id' => $article->id])?>">
                <?php if($article->image):?>
                    <img src="<?=$article->image?>" alt="..." width="150">
                <?php else:?>
                    <img src="<?=Yii::getAlias("@web/img/no_image.png")?>" alt="..." width="150">
                <?php endif?>
            </a>
        </div>
        <div class="media-right">
            <h3 class="media-heading clearfix">
                <a href="<?=Url::to(['view', 'id' => $article->id])?>">
                    <?=Html::encode($article->name)?>
                </a>
            </h3>
            <div class="media-heading article-author">
                <strong><?=$article->author->username?></strong>
                <small>(<?=Yii::$app->formatter->asDatetime($article->updated_at, 'php:d M Y H:i')?>)</small>
            </div>
            <div class="article-rubrics">
                <?php if($article->rubrics):?>
                    <?php foreach ($article->rubrics as $rubric):?>
                        <a href="<?= Url::to(['rubric', 'id' => $rubric->id])?>" class="btn btn-default btn-xs">
                            <?=Html::encode($rubric->name)?>
                        </a>
                    <?php endforeach?>
                <?php else:?>
                    <em class="text-warning">Рубрики не указаны</em>
                <?php endif?>
            </div>
        </div>
    </div>
<?php endforeach?>