<?php
/** @var $rubrics Rubric[] */

use app\models\db\Rubric;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="rubrics">
    <?php foreach ($rubrics as $rubric):?>
        <a href="<?= Url::to(['rubric', 'id' => $rubric->id])?>" class="btn btn-default">
            <?=Html::encode($rubric->name)?>
        </a>
    <?php endforeach?>
</div>
