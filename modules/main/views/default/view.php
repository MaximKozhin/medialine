<?php

use app\models\db\Article;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var $article Article */
?>
<div class="article-view">
    <h1 class="no-margin clearfix"><?=Html::encode($this->title)?></h1>
    <hr>
    <div class="article-author form-group">
        <strong><?=$article->author->username?></strong>
        <small>(<?=Yii::$app->formatter->asDatetime($article->updated_at, 'php:d M Y H:i')?>)</small>
    </div>
    <?php if($article->rubrics):?>
        <div class="article-rubrics form-group">
            <?=$this->render('_rubrics', ['rubrics' => $article->rubrics])?>
        </div>
    <?php endif?>
    <?php if($article->image):?>
        <div class="article-image form-group">
            <img src="<?=$article->image?>" alt="..." class="img-responsive" width="100%">
        </div>
    <?php endif?>
    <div class="article-body">
        <?=$article->body?>
    </div>
</div>
