<?php
/* @var $rubric Rubric */
/* @var $articles Article[] */
/* @var $pagination Pagination */

use app\models\db\Article;
use app\models\db\Rubric;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>

<div class="main-rubric">
    <h1 class="no-margin clearfix"><?=Html::encode($this->title)?></h1>
    <hr>
    <?php if($rubric->children):?>
        <?=$this->render('_rubrics', ['rubrics' => $rubric->children])?>
    <?php endif?>
    <br>
    <?php if($articles):?>
        <div class="article-list" >
            <?=$this->render('_articles', ['articles' => $articles])?>
        </div>
    <?php else:?>
        <div class="alert alert-warning no-margin">Сюда пока никто не добавил новостей</div>
    <?php endif?>
    <div class="text-center">
        <?= LinkPager::widget(['pagination' => $pagination])?>
    </div>
</div>
