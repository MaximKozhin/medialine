<?php

namespace app\modules\admin\controllers;

use app\models\db\ArticleRubric;
use app\models\db\Rubric;
use Yii;
use app\models\db\Article;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Article::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post()) and $model->validate()) {

            /** Устанавливаем превью картинки */
            if($file = UploadedFile::getInstance($model, 'image')) {

                /** Генерируем имя файла */
                $filename = md5(Yii::$app->security->generateRandomString()).".".$file->extension;

                if($uploaded = $file->saveAs(Yii::getAlias("@webroot/upload/{$filename}"))) {
                    $model->image = Yii::getAlias("@web/upload/{$filename}");
                } else {
                    $model->image = $model->oldAttributes['image'];
                }
            } else {
                $model->image = $model->oldAttributes['image'];
            }

            /** Сохраняем */
            if($model->save()) {
                Yii::$app->session->addFlash('success', 'Новость успешно была добавлена');
            } else {
                Yii::$app->session->addFlash('error', 'Ошибка сохранения новости');
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException|Exception if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) and $model->validate()) {

            /** Устанавливаем превью картинки */
            if($file = UploadedFile::getInstance($model, 'image')) {

                /** Генерируем имя файла */
                $filename = md5(Yii::$app->security->generateRandomString()).".".$file->extension;

                if($uploaded = $file->saveAs(Yii::getAlias("@webroot/upload/{$filename}"))) {
                    $model->image = Yii::getAlias("@web/upload/{$filename}");
                } else {
                    $model->image = $model->oldAttributes['image'];
                }
            } else {
                $model->image = $model->oldAttributes['image'];
            }

            /** Сохраняем */
            if($model->save()) {
                Yii::$app->session->addFlash('success', 'Новость успешно была обновлена');
            } else {
                Yii::$app->session->addFlash('error', 'Ошибка обновления новости');
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionRubrics($id)
    {
        $model = $this->findModel($id);

        /** Есть отправленная форма */
        if (Yii::$app->request->isPost) {

            /** Смотрим переданные рубрики */
            if($_POST['rubrics']) {

                /** Удаляем все рубрики */
                ArticleRubric::deleteAll(['article_id' => $model->id]);

                /** Перебираем полученные рубрики как ID рубрики => 1/0 и сохраняем, если рубрика выбрана */
                foreach ($_POST['rubrics'] as $rubric_id => $value) {

                    /** Добавляем привязку статьи к рубрике */
                    if($value and (new ArticleRubric(['article_id' => $model->id, 'rubric_id' => $rubric_id]))->save()) {
                        Yii::$app->session->addFlash('success', 'Рубрика успешно добавлена');
                    }
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        /** Массив рубрик для привязки */
        $rubrics = Rubric::find()->select(['name'])->where(['deleted' => false])->indexBy('id')->column();

        return $this->render('rubrics', [
            'model' => $model,
            'rubrics' => $rubrics,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
