<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\db\Rubric;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * RubricController implements the CRUD actions for Rubric model.
 */
class RubricController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rubric models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Rubric::find()->with('parent'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rubric model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rubric model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws Exception
     */
    public function actionCreate()
    {
        $model = new Rubric();

        if ($model->load(Yii::$app->request->post()) and $model->validate()) {

            /** Устанавливаем превью картинки */
            if($file = UploadedFile::getInstance($model, 'image')) {

                /** Генерируем имя файла */
                $filename = md5(Yii::$app->security->generateRandomString()).".".$file->extension;

                if($uploaded = $file->saveAs(Yii::getAlias("@webroot/upload/{$filename}"))) {
                    $model->image = Yii::getAlias("@web/upload/{$filename}");
                } else {
                    $model->image = $model->oldAttributes['image'];
                }
            } else {
                $model->image = $model->oldAttributes['image'];
            }

            /** Сохраняем */
            if($model->save()) {
                Yii::$app->session->addFlash('success', 'Рубрика успешно была добавлена');
            } else {
                Yii::$app->session->addFlash('error', 'Ошибка сохранения рубрики');
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'rubrics' => $this->findRubrics()
        ]);
    }

    /**
     * Updates an existing Rubric model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) and $model->validate()) {

            /** Устанавливаем превью картинки */
            if($file = UploadedFile::getInstance($model, 'image')) {

                /** Генерируем имя файла */
                $filename = md5(Yii::$app->security->generateRandomString()).".".$file->extension;

                if($uploaded = $file->saveAs(Yii::getAlias("@webroot/upload/{$filename}"))) {
                    $model->image = Yii::getAlias("@web/upload/{$filename}");
                } else {
                    $model->image = $model->oldAttributes['image'];
                }
            } else {
                $model->image = $model->oldAttributes['image'];
            }

            /** Сохраняем */
            if($model->save()) {
                Yii::$app->session->addFlash('success', 'Рубрика успешно была обновлена');
            } else {
                Yii::$app->session->addFlash('error', 'Ошибка обновления рубрики');
            }

            return $this->redirect(['view', 'id' => $model->id]);

        }

        return $this->render('update', [
            'model' => $model,
            'rubrics' => $this->findRubrics()
        ]);
    }

    /**
     * Deletes an existing Rubric model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rubric model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rubric the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rubric::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Получение списка рубрик для передачи в представление
     * @return array
     */
    protected function findRubrics()
    {
        return Rubric::find()->select(['name'])->indexBy('id')->where(['deleted' => false])->column();
    }
}
