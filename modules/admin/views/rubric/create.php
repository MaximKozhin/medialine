<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Rubric */
/* @var $rubrics array */

$this->title = 'Добавить рубрику';
$this->params['breadcrumbs'][] = ['label' => 'Рубрики', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubric-create">
    <h1 class="no-margin clearfix">
        <?= Html::encode($this->title) ?>
    </h1>
    <hr>
    <div style="overflow: auto">
        <?= $this->render('_form', [
            'model' => $model,
            'rubrics' => $rubrics
        ]) ?>
    </div>
</div>
