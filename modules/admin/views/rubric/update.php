<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Rubric */
/* @var $rubrics array */

$this->title = 'Редактировать рубрику';
$this->params['breadcrumbs'][] = ['label' => 'Рубрики', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="rubric-update">
    <h1 class="no-margin clearfix">
        <?= Html::encode($this->title) ?>
    </h1>
    <hr>
    <div style="overflow: auto">
        <?= $this->render('_form', [
            'model' => $model,
            'rubrics' => $rubrics
        ]) ?>
    </div>
</div>
