<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model app\models\db\Article */
/* @var $rubrics array */

$this->title = 'Рубрики статьи';
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-rubrics">
    <h1 class="no-margin clearfix"><?=$this->title?></h1>
    <hr>
    <?php ActiveForm::begin()?>
        <?php foreach ($rubrics as $id => $name):?>
            <div class="form-group">
                <?=Html::checkbox("rubrics[{$id}]", key_exists($id, $model->rubrics), [
                    'label' => $name
                ])?>
            </div>
        <?php endforeach;?>
        <div class="form-group">
            <?= Html::submitButton('Применить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end()?>
</div>


