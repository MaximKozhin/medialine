<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\db\Article */

$this->title = 'Добавить новость';
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-create">
    <h1 class="no-margin clearfix">
        <?= Html::encode($this->title) ?>
    </h1>
    <hr>
    <div style="overflow: auto">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
