<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\db\Article */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <h1 class="no-margin clearfix">
        <?= Html::encode($this->title) ?>
        <div class="pull-right">
            <a href="<?=Url::to(['update', 'id' => $model->id])?>" class="btn btn-primary">
                <span class="glyphicon glyphicon-pencil"></span>
                <span class="hidden-xs hidden-sm">Редактировать</span>
            </a>
            <a href="<?=Url::to(['rubrics', 'id' => $model->id])?>" class="btn btn-info">
                <span class="glyphicon glyphicon-tags"></span>
                <span class="hidden-xs hidden-sm">Рубрики</span>
            </a>
            <a href="<?=Url::to(['delete', 'id' => $model->id])?>" class="btn btn-danger" data-method="POST" data-confirm="Удалить?">
                <span class="glyphicon glyphicon-trash"></span>
                <span class="hidden-xs hidden-sm">Удалить</span>
            </a>
        </div>
    </h1>
    <hr>
    <div style="overflow: auto">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'author_id',
                    'format' => 'html',
                    'value' => $model->author_id
                        ? Html::a($model->author->username, ['identity/view', 'id' => $model->author_id])
                        : null,
                ],
                [
                    'attribute' => 'image',
                    'format' => 'raw',
                    'value' => function($model) {
                        return Html::a($model->image, [$model->image], ['target' => 'blank']);
                    }
                ],
                'name',
                'body:html',
                'deleted:boolean',
                'created_at:datetime',
                'updated_at:datetime',
            ],
        ]) ?>
    </div>

</div>
