<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1 class="no-margin clearfix">
        <?= Html::encode($this->title) ?>
        <div class="pull-right">
            <a href="<?=Url::to(['create'])?>" class="btn btn-success">
                <span class="glyphicon glyphicon-plus"></span>
                <span class="hidden-xs hidden-sm">Добавить новость</span>
            </a>
        </div>
    </h1>
    <hr>
    <div style="overflow: auto;">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'rowOptions' => function ($model){
                return $model->deleted ? ['class' => 'danger'] : [];
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'name',
                [
                    'attribute' => 'author_id',
                    'format' => 'raw',
                    'value' => function($model) {
                        return Html::a($model->author->username, ['identity/view', 'id' => $model->author_id]);
                    }
                ],
                'deleted:boolean',
                'created_at:datetime',
                'updated_at:datetime',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>


</div>
