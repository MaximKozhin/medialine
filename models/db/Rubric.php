<?php

namespace app\models\db;

use app\models\interfaces\ParentInterface;
use app\models\traits\NonDeletableTrait;
use app\validators\ParentValidator;
use yii\base\InvalidConfigException;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%rubric}}".
 *
 * @property int $id
 * @property int|null $parent_id Родительская рубрика
 * @property int|null $level Уровень вложенности
 * @property string|null $image Изображение рубрики
 * @property string $name Наименование рубрики
 * @property int|null $deleted Удалено
 * @property int|null $created_at Создано
 * @property int|null $updated_at Изменено
 *
 * @property Rubric $parent
 * @property Rubric[] $children
 * @property RubricTree[] $parentTree
 * @property RubricTree[] $rubricTree
 */
class Rubric extends ActiveRecord implements ParentInterface
{
    /** Строки не удаляются из БД */
    use NonDeletableTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%rubric}}';
    }

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at'
            ],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['level'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['level'],
                ],
                'value' => function() {
                    return 1 + (int)$this->parent->level;
                }
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'level', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'required'],
            [['name'], 'trim'],
            [['image'], 'image',
                'minWidth' => 100, 'maxWidth' => 1920, 'minHeight' => 100, 'maxHeight' => 1024,
                'extensions' => 'png, jpeg, jpg'],
            [['image', 'name'], 'string', 'max' => 255],
            ['image', 'default', 'value' => null],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rubric::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['parent_id'], ParentValidator::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родительская рубрика',
            'level' => 'Уровень вложенности',
            'image' => 'Изображение рубрики',
            'name' => 'Наименование рубрики',
            'deleted' => 'Удалено',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        /** Удаляем из таблицы дерева все строки с рубрикой */
        RubricTree::deleteAll(['rubric_id' => $this->id]);

        /** Родительская рубрика */
        $parent = $this->parent;

        /** Пока родительская рубрика есть */
        while ($parent) {

            /** Сохраняем ссылки в таблицк дерева, переходя на уровень выше */
            if((new RubricTree(['rubric_id' => $this->id, 'parent_id' => $parent->id]))->save()) {
                $parent = $parent->parent;
            } else {
                break;
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Gets query for [[Articles]].
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getArticles()
    {
        return $this->hasMany(Article::class, ['id' => 'article_id'])
            ->viaTable('{{%article_rubric}}', ['rubric_id' => 'id'])
            ->where(['deleted' => false]);
    }

    /**
     * @return string
     */
    public function getPrimaryAttribute(): string
    {
        return 'id';
    }

    /**
     * @return string
     */
    public function getParentAttribute(): string
    {
        return 'parent_id';
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Rubric::class, [$this->getPrimaryAttribute() => $this->getParentAttribute()]);
    }

    /**
     * Gets query for [[Rubrics]].
     *
     * @return ActiveQuery|array
     */
    public function getChildren()
    {
        return $this->hasMany(Rubric::class, [$this->getParentAttribute() => $this->getPrimaryAttribute()])
            ->where(['deleted' => false]);
    }

    /**
     * Gets query for [[RubricTrees]].
     *
     * @return ActiveQuery
     */
    public function getParentTree()
    {
        return $this->hasMany(RubricTree::class, ['rubric_id' => 'id']);
    }

    /**
     * Gets query for [[RubricTrees]].
     *
     * @return ActiveQuery
     */
    public function getRubricTree()
    {
        return $this->hasMany(RubricTree::class, ['parent_id' => 'id']);
    }
}
