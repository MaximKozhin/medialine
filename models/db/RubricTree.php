<?php

namespace app\models\db;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%rubric_tree}}".
 *
 * @property int $rubric_id Рубрика
 * @property int $parent_id Родительская рубрика
 *
 * @property Rubric $parent
 * @property Rubric $rubric
 */
class RubricTree extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%rubric_tree}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rubric_id', 'parent_id'], 'required'],
            [['rubric_id', 'parent_id'], 'integer'],
            [['rubric_id', 'parent_id'], 'unique', 'targetAttribute' => ['rubric_id', 'parent_id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rubric::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['rubric_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rubric::class, 'targetAttribute' => ['rubric_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rubric_id' => 'Рубрика',
            'parent_id' => 'Родительская рубрика',
        ];
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Rubric::class, ['id' => 'parent_id']);
    }

    /**
     * Gets query for [[Rubric]].
     *
     * @return ActiveQuery
     */
    public function getRubric()
    {
        return $this->hasOne(Rubric::class, ['id' => 'rubric_id']);
    }
}
