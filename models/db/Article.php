<?php

namespace app\models\db;

use app\models\traits\NonDeletableTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%article}}".
 *
 * @property int $id
 * @property int|null $author_id Автор статьи
 * @property string|null $image Изображение статьи
 * @property string $name Заголовок статьи
 * @property string $body Тело статьи
 * @property int|null $deleted Удалено
 * @property int|null $created_at Создано
 * @property int|null $updated_at Изменено
 *
 * @property Identity $author
 * @property Rubric[] $rubrics
 */
class Article extends ActiveRecord
{
    /** Строки не удаляются из БД */
    use NonDeletableTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at'
            ],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['author_id'],
                ],
                'value' => Yii::$app->user->id
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_id', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'required'],
            [['name'], 'trim'],
            [['image', 'name'], 'string', 'max' => 255],
            [['body'], 'string'],
            [['image'], 'default', 'value' => null],
            [['image'], 'image',
                'minWidth' => 100, 'maxWidth' => 1920, 'minHeight' => 100, 'maxHeight' => 1024,
                'extensions' => 'png, jpeg, jpg'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Identity::class, 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Автор статьи',
            'image' => 'Изображение статьи',
            'name' => 'Заголовок статьи',
            'body' => 'Тело статьи',
            'deleted' => 'Удалено',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
        ];
    }

    /**
     * Gets query for [[Author]].
     *
     * @return ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Identity::class, ['id' => 'author_id']);
    }

    /**
     * Gets query for [[Rubrics]].
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getRubrics()
    {
        return $this->hasMany(Rubric::class, ['id' => 'rubric_id'])
            ->viaTable('{{%article_rubric}}', ['article_id' => 'id'])
            ->where([Rubric::tableName().'.deleted' => false])
            ->indexBy('id');
    }
}
