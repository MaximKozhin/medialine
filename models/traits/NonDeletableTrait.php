<?php

namespace app\models\traits;

/**
 * Trait NonDeletableTrait
 * @package app\models\traits
 */
trait NonDeletableTrait
{
    /**
     * @return bool
     */
    public function delete()
    {
        $this->deleted = 1;
        return $this->save();
    }

    /**
     * @param null $condition
     * @param array $params
     * @return integer
     */
    public static function deleteAll($condition = null, $params = [])
    {
        return static::updateAll(['deleted' => 1], $condition, $params);
    }
}