<?php

namespace app\models\interfaces;

use yii\db\ActiveQuery;

/**
 * Interface ParentInterface
 * @package app\models\interfaces
 */
interface ParentInterface
{
    /**
     * @return string
     */
    public function getPrimaryAttribute() : string;

    /**
     * @return string
     */
    public function getParentAttribute() : string;

    /**
     * @return $this|ActiveQuery
     */
    public function getParent();

    /**
     * @return self[]|ActiveQuery
     */
    public function getChildren();
}