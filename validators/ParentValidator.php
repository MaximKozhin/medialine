<?php

namespace app\validators;

use app\models\interfaces\ParentInterface;
use yii\base\Model;
use yii\db\Exception;
use yii\validators\Validator;

/**
 * Class CountryValidator
 * @package app\validators
 */
class ParentValidator extends Validator
{
    /**
     * @param Model|ParentInterface $model
     * @param string $attribute
     * @throws Exception
     */
    public function validateAttribute($model, $attribute)
    {
        /** Проверяем тип */
        if(!($model instanceof ParentInterface)) {
            throw new Exception("Модель должна реализовать родительский интерфейс 'ParentInterface'");
        }

        /** @var string $primaryAttribute */
        $primaryAttribute = $model->getPrimaryAttribute();

        /**
         * Масиив идентификаторов
         * @var int[] $identifiers
         */
        $identifiers = [];

        /** Если у модели уже есть идентификатор, то добавляем его в цепочку идентификаторов */
        if($model->$primaryAttribute) {
            $identifiers[] = $model->$primaryAttribute;
        }

        /** @var $this $parent */
        $parent = $model->getParent()->one();

        /** Пока есть цепочка родительских элементов */
        while ($parent) {

            /** Если родительский идентификатор есть в цепочке, то получаются ссылки по вложенности закольцованы */
            if(in_array($parent->$primaryAttribute, $identifiers)) {
                $model->addError($attribute, 'Вложенность указывает сама на себя');
                break;
            } else {
                /** Добавляем идентификатор в цепочку и идем уровнем выше */
                $identifiers[] = $parent->$primaryAttribute;
                $parent = $parent->getParent()->one();
            }
        }
    }
}